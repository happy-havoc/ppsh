import configparser
import os
import pathlib

from cmd2 import Cmd, plugin
import functools
from shipp_config import ShippConfig
from colorama import Fore, Back


class Ppsh(Cmd):
    def __init__(self):
        Cmd.__init__(self)
        self.register_precmd_hook(self.emptyLineBeforeCmdHook)
        self.register_postcmd_hook(self.emptyLineAfterCmdHook)

    # ** custom ppsh vars **
    ppshVerboseName = "Pure Python Shell"
    ppshCurrentDevVersion = "0.01"
    user_name = "flux"

    # ** completion **
    complete_what = functools.partialmethod(Cmd.path_complete, dir_only=True)
    complete_go = functools.partialmethod(Cmd.path_complete, dir_only=True)
    complete_open = Cmd.path_complete

    # ** cmd vars **
    intro = f"Welcome to the {ppshVerboseName} v {ppshCurrentDevVersion}\n"
    prompt_color_on = Fore.GREEN + Back.BLACK
    prompt_color_off = Fore.RESET + Back.RESET
    prompt_path_color_on = Fore.CYAN + Back.BLACK
    prompt_path_color_off = Fore.RESET + Back.RESET
    prompt = prompt_color_on + f"{user_name} < " + prompt_color_off + prompt_path_color_on + f"{os.getcwd()}" \
             + prompt_path_color_off + prompt_color_on + " >\n λ " + prompt_color_off
    file = None

    # ** custom data vars **
    FRIENDS = ["Marc", "Matze", "Fabio"]

    def do_parrot(self, arg):
        if arg:
            print(arg)
        else:
            self.help_echo()

    def help_parrot(self):
        print("echo <argument>\n"
              + "Return the argument to test the console output")

    def complete_hello(self, text, line, begidx, endidx):
        if not text:
            completions = self.FRIENDS[:]
        else:
            completions = [f
                           for f in self.FRIENDS
                           if f.startswith(text)]
        return completions

    def update_prompt(self, username, path):
        self.prompt = self.prompt_color_on + f"{username} < " + self.prompt_color_off + self.prompt_path_color_on \
                      + f"{path}" + self.prompt_path_color_off + self.prompt_color_on + " >\n λ " \
                      + self.prompt_color_off

    def do_where(self, arg):
        """Print the current working directory"""
        print(os.getcwd())

    def do_go(self, arg):
        if arg:
            path = str(arg).replace("\"", "")
            try:
                os.chdir(path)
                self.update_prompt(self.user_name, os.getcwd())
            except Exception:
                print("Error: no directory")
        else:
            print("no path argument")

    def help_go(self):
        print("go <dir>\n"
              "Change the working directory")

    def do_exit(self, line):
        """Quits the Ppsh"""
        return True

    def do_what(self, args):
        if len(str(args).split(" ")) > 1:
            self.help_ls()
            return
        elif args != "":
            root_path = str(args).split(" ")[0]
            content = os.listdir(root_path)
        elif args == "":
            content = os.listdir(os.getcwd())
            root_path = os.getcwd()
        else:
            self.help_ls()
            return

        for item in content:
            size = str(os.path.getsize(f"{root_path}{os.sep}{item}"))
            if os.path.isdir(f"{root_path}{os.sep}{item}"):
                type = "d"
            elif os.path.isfile(f"{root_path}{os.sep}{item}"):
                type = "f"
            else:
                type = "x"
            print(size.rjust(15, " "), "B  ", type, item.ljust(10, " "))
        print()

    def help_what(self):
        print("what [path]\nPrint the content of the current working directory")

    def do_username(self, line):
        if not line:
            self.help_username()
            return
        else:
            username = str(line).split(" ")[0]
            self.user_name = username
            self.update_prompt(self.user_name, os.getcwd())
            print(f"User name set to {username}")

    def help_username(self):
        print("username <username>\n"
              "Set the username of the shell user")

    def do_info(self, line):
        print("+==========================================+")
        print(f"{self.ppshVerboseName.upper()} {self.ppshCurrentDevVersion.upper()}")
        print("+==========================================+")

    def do_open(self, line):
        if line:
            try:
                os.system(f"start {line}")
            except Exception:
                print("Error: could not open the file")

    def do_ini(self, line):
        if not line:
            self.help_ini()
            return
        arg_list = str(line).split(" ")
        arg_length = len(arg_list)
        if arg_length > 1:
            print(f"too many positional arguments ({arg_length})\n")
            self.help_ini()
            return
        print(self.shipp_config.get_val(arg_list[0]))

    def complete_ini(self, text, line, begidx, endidx):
        if not text:
            completions = self.shipp_config.ini_arg_names[:]
        else:
            completions = [f
                           for f in self.shipp_config.ini_arg_names
                           if f.startswith(text)]
        return completions

    def help_ini(self):
        print("ini <arg>\n"
              "Print the value of the ini key")

    # ** pre command hooks **

    def emptyLineBeforeCmdHook(self, data: plugin.PrecommandData) -> plugin.PrecommandData:
        print()
        return data

    def emptyLineAfterCmdHook(self, data: plugin.PostcommandData) -> plugin.PostcommandData:
        print()
        return data

    # ** overwritten functions **

    def preloop(self):
        # run and parse the config
        run_from_ini = self.read_config()
        if not run_from_ini:
            print("! INFO: Could not read shipp.ini. Falling back to default configuration")
            self.run_from_ini = run_from_ini
        elif run_from_ini:
            self.run_from_ini = run_from_ini
        else:
            self.run_from_ini = False
        # ** setup prompt **
        if self.shipp_config.is_set("username"):
            self.update_prompt(self.shipp_config.get_val("username"), os.getcwd())
        # ** setup home dir **
        if self.shipp_config.is_set("user_home_dir"):
            # if user_home_dir = yes overwrite home_dir param
            if self.shipp_config.get_val("user_home_dir") == "yes":
                home_dir = pathlib.Path.home()
            # if user_home_dir = no use the home dir specified in home_dir
            elif self.shipp_config.get_val("user_home_dir") == "no":
                if self.shipp_config.is_set("home_dir"):
                    specified_home_dir = self.shipp_config.get_val("home_dir")
                    if os.path.isdir(specified_home_dir):
                        home_dir = specified_home_dir
                    else:
                        print(f"{specified_home_dir} is no dir")
            if home_dir is None or home_dir == "":
                home_dir = os.getcwd()
            self.update_prompt(
                self.shipp_config.get_val("username") if self.shipp_config.is_set("username") else "shipp",
                home_dir
            )

    def read_config(self):
        self.shipp_config = ShippConfig()
        runconfig = configparser.ConfigParser()
        runconfig.read("shipp.ini")
        try:
            run_from_config = runconfig["start"]["use_ini"]
        except KeyError:
            self.run_from_default_config()
            return False
        # fallback to default configuration if start.use_ini was either no or something we did not understand
        if run_from_config == "no" or run_from_config != "yes":
            self.run_from_default_config()
            return False
        elif run_from_config == "yes":
            self.run_from_user_config(runconfig)
            return True

    def run_from_default_config(self):
        self.shipp_config.set_ini_var("use_ini", "no")
        self.shipp_config.set_ini_var("username", "shipp")

    def run_from_user_config(self, config):
        startIniSection = config["start"]
        self.shipp_config.set_ini_var("use_ini",
                                      startIniSection.get("use_ini", "yes"))
        self.shipp_config.set_ini_var("username",
                                      startIniSection.get("username", "shipp"))
        self.shipp_config.set_ini_var("home_dir",
                                      startIniSection.get("home_dir", os.getcwd()))
        self.shipp_config.set_ini_var("user_home_dir",
                                      startIniSection.get("user_home_dir", "no"))

    def do_shell(self, line):
        """Open a pipe to the OS cmd and send the command through that pipe."""
        output = os.popen(line).read()
        print(output)

    def postloop(self):
        print


if __name__ == '__main__':
    app = Ppsh()
    app.cmdloop()

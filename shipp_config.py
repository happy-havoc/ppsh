class ShippConfig:

    ini_arg_names = []
    ini_arg_vars = {}

    def set_ini_var(self, arg_name, arg_val):
        self.ini_arg_names.append(arg_name)
        self.ini_arg_vars[arg_name] = arg_val

    def get_verbose_val(self, key):
        arg_val = self.get_val(key)
        if arg_val == None or arg_val == "":
            return f"ini: {key} not set"
        else:
            return f"ini: {key} --> {arg_val}"

    def get_val(self, key):
        arg_val = self.ini_arg_vars.get(key)
        if arg_val == None or arg_val == "":
            return ""
        else:
            return arg_val

    def is_set(self, key):
        arg_val = self.ini_arg_vars.get(key)
        if arg_val == None or arg_val == "":
            return False
        else:
            return True